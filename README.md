# dwm - dynamic window manager
> **Note**: This config is outdated and archived. See all my config and dotfiles on [this new repo](https://codeberg.org/SnowCode/rice).

![screenshot](./screenshot.png)

`dwm` is an extremely fast, small, and dynamic window manager for X.

## Features
* Shortcuts built for AZERTY keyboards 
* Indicators in the status bar for CPU usage, memory usage, disk usage, sound volume, battery, network connection and date. 
* Nice icons for the status bar 
* Gaps to make the whole thing better 
* Solarized color scheme 
* Alacritty as default terminal emulator 
* Brightness control keys support 
* Sound control keys support

## Requirements
* Xlib header files 
* FontAwesome (you may need to rename the font in the config file) 
* Feh (for the wallpaper) 
* mpstat (for the CPU indicator)

## Installation
First clone the repository

```bash 
git clone https://codeberg.org/SnowCode/dwm 
cd dwm 
```

Then, install it

```bash 
sudo make clean install 
```

Finally, add the [start_dwm](./start_dwm) file content to your `.xinitrc` file.

## Customize
This repo isn't made to be installed as said above, it's meant to be implemented as 
patches to your own.

```bash
# Get gaps
git diff ec1528c49a~1 ec1528c49a

# Support icons (require fontawesome)
git diff 547c8c11cd~1 9a2f4a47de

# Support another terminal than ST
git diff 9444534123~1 9444534123

# Support volume controls
git diff 1a1f134fbe~1 1a1f134fbe

# Support brightness controls
git diff 1476787b60~1 1476787b60

# Configure tags and shortcuts
git diff d8869c7b5b~1 d8869c7b5b

# Change the color scheme to solarized-dark
git diff 74d2a15ccc~1 74d2a15ccc

# Create a status bar and a start script for dwm
cat start_dwm
```
